package com.example.tp3;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Parcelable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tp3.data.Match;
import com.example.tp3.data.SportDbHelper;
import com.example.tp3.data.Team;

import com.example.tp3.data.Match;
import com.example.tp3.data.Team;
import com.example.tp3.webservice.WebServiceUrl;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
//import fr.uavignon.shuet.tp3.webservice.WebServiceGet;

public class TeamActivity extends AppCompatActivity {

    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;

    private boolean setImage = false;
    private ImageView imageBadge;
    private Team team;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        team = (Team) getIntent().getParcelableExtra(Team.TAG);

        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textLeague = (TextView) findViewById(R.id.league);
        textStadium = (TextView) findViewById(R.id.editStadium);
        textStadiumLocation = (TextView) findViewById(R.id.editStadiumLocation);
        textTotalScore = (TextView) findViewById(R.id.editTotalScore);
        textRanking = (TextView) findViewById(R.id.editRanking);
        textLastMatch = (TextView) findViewById(R.id.editLastMatch);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageBadge = (ImageView) findViewById(R.id.imageView);

        final Button but = (Button) findViewById(R.id.button);
        updateView();

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Worker process = new Worker();
                process.execute();
            }
        });

    }

    @Override
    public void onBackPressed() {
        if(team != null) {
            Intent intent = new Intent(TeamActivity.this, MainActivity.class);
            intent.putExtra(team.TAG, (Parcelable) team);
            setResult(RESULT_OK, intent);
            finish();
        }
        super.onBackPressed();
        Toast.makeText(this,"Back key pressed", Toast.LENGTH_SHORT).show();
    }

    private void updateView() {

        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        if(team.getRanking() != 0) textRanking.setText(Integer.toString(team.getRanking()));
        if(!team.getLastEvent().toString().equals("null : 0-0")) textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());
        File path = new File(this.getExternalFilesDir(null), team.getName()+".png");
        Bitmap myBitmap = BitmapFactory.decodeFile(path.getAbsolutePath());
        imageBadge.setImageBitmap(myBitmap);
    }

    private void saveToInternalStorage(Bitmap icon)
    {
        File path = new File(this.getExternalFilesDir(null), team.getName()+".png");
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(path);
            // Use the compress method on the BitMap object to write image to the OutputStream
            icon.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.flush();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public class Worker extends AsyncTask<TextView, String, Team> implements com.example.tp3.Worker {

        Request request;
        AlertDialog.Builder cancelledAlert;
        String TAG;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            cancelledAlert = new AlertDialog.Builder(TeamActivity.this);
        }

        //Background thread
        @Override
        protected Team doInBackground(TextView... textViews) {
            request = new Request();
            boolean run = true;
            while(run == true)
            {
                Team tempTeam = team;
                tempTeam = request.getTeamInfo(tempTeam);
                if(tempTeam == null)
                {
                    this.cancel(true);
                    if(isCancelled()) {
                        break;
                    }
                }
                //test = request.getImageFromURL(team.getTeamBadge());
                saveToInternalStorage(request.getImageFromURL(team.getTeamBadge()));
                setImage = true;
                team = request.getMatchInfo(tempTeam);
                team = request.getRankingInfo(team);
                run = false;
            }
            return team;
        }

        //UI thread
        @Override
        protected void onPostExecute(Team team) {
            //new GetBitmap().execute();
            updateView();
            super.onPostExecute(team);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            cancelledAlert.setMessage("Cette équipe n'existe pas veuillez la supprimer").create().show();
        }
    }
}