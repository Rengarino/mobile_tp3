package com.example.tp3;

import android.util.JsonReader;
import android.util.JsonToken;

import com.example.tp3.data.Match;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;



public class JSONResponseHandlerMatch {


        private static final String TAG = com.example.tp3.JSONResponseHandlerTeam.class.getSimpleName();

        private Match match;


        public JSONResponseHandlerMatch(Match match) {
            this.match = match;
        }

        /**
         * @param response done by the Web service
         * @return A Match with attributes filled with the collected information if response was
         * successfully analyzed
         */
        public void readJsonStream(InputStream response) throws IOException {
            JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
            try {
                readEvent(reader);
            } finally {
                reader.close();
            }
        }

        public void readEvent(JsonReader reader) throws IOException {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("results")) { //Le nom du tableau pour la requête eventslast.php
                    readArrayEvent(reader);
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
        }


        private void readArrayEvent(JsonReader reader) throws IOException {
            reader.beginArray();
            int nb = 0; // only consider the first element of the array
            while (reader.hasNext() ) {
                reader.beginObject();
                while (reader.hasNext()) {
                    String name = reader.nextName();
                    if (nb==0) {
                        if (name.equals("idEvent")) {
                            match.setId(reader.nextLong());
                        } else if (name.equals("strEvent")) {
                            match.setLabel(reader.nextString());
                        } else if (name.equals("strHomeTeam")) {
                            match.setHomeTeam(reader.nextString());
                        } else if (name.equals("strAwayTeam")) {
                            match.setAwayTeam(reader.nextString());
                        } else if (name.equals("intHomeScore")) {
                            if(reader.peek() == JsonToken.NULL) {
                                match.setHomeScore(-1); //Set -1 if error with value
                                reader.skipValue();
                            }
                            else {
                                match.setHomeScore(reader.nextInt());
                            }
                        } else if (name.equals("intAwayScore")) {
                            if(reader.peek() == JsonToken.NULL) {
                                match.setAwayScore(-1);
                                reader.skipValue();
                            }
                            else {
                                match.setAwayScore(reader.nextInt());
                            }
                        } else {
                            reader.skipValue();
                        }
                    }  else {
                        reader.skipValue();
                    }
                }
                reader.endObject();
                nb++;
            }
            reader.endArray();
        }

    }
