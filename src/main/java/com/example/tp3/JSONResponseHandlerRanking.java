package com.example.tp3;

import android.util.JsonReader;

import com.example.tp3.data.Team;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JSONResponseHandlerRanking {

    private static final String TAG = com.example.tp3.JSONResponseHandlerTeam.class.getSimpleName();

    private Team team;


    public JSONResponseHandlerRanking(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readRanking(reader);
        } finally {
            reader.close();
        }
    }

    public void readRanking(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("table")) { //Le nom du tableau pour la requête eventslast.php
                readArrayRanking(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayRanking(JsonReader reader) throws IOException {
        reader.beginArray();
        int nb = 0; // only consider the first element of the array
        while (reader.hasNext() ) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("teamid"))
                {
                    long id = reader.nextLong();
                    if (id == team.getIdTeam()) {
                        team.setRanking(nb+1);
                        while(reader.hasNext())
                        {
                            String s = reader.nextName();
                            if(s.equals("total"))
                            {
                                team.setTotalPoints(reader.nextInt());
                            }
                            else {
                                reader.skipValue();
                            }
                        }
                    }
                }
                else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
    }
}
