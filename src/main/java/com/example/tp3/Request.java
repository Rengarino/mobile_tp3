package com.example.tp3;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.tp3.data.Team;
import com.example.tp3.webservice.WebServiceUrl;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Request {
    JSONResponseHandlerMatch JSONmatch;
    JSONResponseHandlerRanking JSONranking;
    JSONResponseHandlerTeam JSONteam;

    public Team getTeamInfo(Team team) {
        JSONteam = new JSONResponseHandlerTeam(team);
        URL url = null;

        try {
            url = WebServiceUrl.buildSearchTeam(team.getName());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection httpURLConnection = null; //This is the connection
        try {
            httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream(); //Stream let us read or write data of this connection
            JSONteam.readJsonStream(inputStream); //Now how do I retrieve the team filled

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            httpURLConnection.disconnect();
            if(!JSONteam.complete)
            {
                return null;
            }
        }
        //Return the team
        return team;
    }

    public Bitmap getImageFromURL(String link)
    {
        //Set the image and save it
        Bitmap icon = null;
        HttpURLConnection httpURLConnection = null;
        try {
            URL url2 = new URL(link);
            httpURLConnection = (HttpURLConnection) url2.openConnection();
            InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
            icon = BitmapFactory.decodeStream(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            httpURLConnection.disconnect();
        }
        return icon;
    }

    public Team getMatchInfo(Team team) {
        JSONmatch= new JSONResponseHandlerMatch(team.getLastEvent());
        URL url = null;

        try {
            url = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection httpURLConnection = null; //This is the connection
        try {
            httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream(); //Stream let us read or write data of this connection
            JSONmatch.readJsonStream(inputStream); //Now how do I retrieve the team filled

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            httpURLConnection.disconnect();
        }
        return team;
    }

    public Team getRankingInfo(Team team) {
        JSONranking = new JSONResponseHandlerRanking(team);
        URL url = null;

        try {
            url = WebServiceUrl.buildGetRanking(team.getIdLeague());

        } catch(MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            JSONranking.readJsonStream(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            httpURLConnection.disconnect();
        }
        return team;
    }

}
