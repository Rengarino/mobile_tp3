package com.example.tp3;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tp3.data.SportDbHelper;
import com.example.tp3.data.Team;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public RecyclerView recyclerMatch;
    public List<Team> listTeam;
    SportDbHelper SDH;
    public CursorAdapter adapter = new CursorAdapter();
    public SwipeRefreshLayout SRL;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SDH = new SportDbHelper(this);
        SDH.dropTable(SDH.getReadableDatabase());
        SDH.populate();
        listTeam = SDH.getAllTeams();

        Cursor cursor = SDH.fetchAllTeams();

        recyclerMatch = (RecyclerView) findViewById(R.id.recyclerTeam);
        recyclerMatch.setLayoutManager(new LinearLayoutManager(this));
        recyclerMatch.addItemDecoration(new DividerItemDecoration(this, LinearLayoutCompat.VERTICAL));
        adapter.changeCursor(cursor);
        recyclerMatch.setAdapter(adapter);
        SRL = findViewById(R.id.swipe_container);
        SRL.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Worker().execute();
            }
        });

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                SDH.deleteTeam((Integer) viewHolder.itemView.getTag());
                Cursor cursor = SDH.fetchAllTeams();
                adapter.changeCursor(cursor);
            }
        }).attachToRecyclerView(recyclerMatch); //Lié la recycler view au item touch helper

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int numberOfTeam = listTeam.size();
                int newID = numberOfTeam + 1;
                Intent intent = new Intent(MainActivity.this, NewTeamActivity.class);
                intent.putExtra("teamID", newID);
                startActivityForResult(intent, 1);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            if (requestCode == 1) { //Requestcode de l'action d'ajout

                if (resultCode == NewTeamActivity.RESULT_OK) {
                    //Récupérer la team et l'ajouter à la BDD
                    Team team = data.getParcelableExtra(Team.TAG);
                    SDH.addTeam(team);
                    listTeam.add(team);
                    Cursor cursor = SDH.fetchAllTeams();
                    adapter.changeCursor(cursor);
                }

            } else if (requestCode == 2) { //Requestcode de TeamActivity
                if (resultCode == TeamActivity.RESULT_OK) {
                    Team team = data.getParcelableExtra(Team.TAG);
                    SDH.updateTeam(team);
                    Cursor cursor = SDH.fetchAllTeams();
                    adapter.changeCursor(cursor);
                }
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }



    class CursorAdapter extends RecyclerView.Adapter<RowHolder> {

        private Cursor adapterCursor;

        public Cursor changeCursor(Cursor cursor) {
            if (cursor == adapterCursor) {
                return null;
            } else {
                Cursor oldCursor = adapterCursor;
                if (oldCursor != null) oldCursor.close();
                adapterCursor = cursor;
                notifyDataSetChanged();
                return adapterCursor;
            }
        }

        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return (new RowHolder(getLayoutInflater()
                    .inflate(R.layout.row_team, parent, false)));
        }

        @Override
        public void onBindViewHolder(RowHolder holder, int position) {
            if (!adapterCursor.moveToPosition(position)) //If the cursor can move to this position then bindModel
            {
                throw new IllegalStateException("Couldn't move cursor to position " + position);
            }
            //Pour identifier l'item dans MainActivity
            int ID = adapterCursor.getInt(adapterCursor.getColumnIndex(SDH._ID));
            holder.itemView.setTag(ID);
            holder.bindModel(adapterCursor);
        }

        @Override
        public int getItemCount() {
            return (adapterCursor.getCount());
        }

    }

    class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView teamView = null;
        TextView match = null;
        ImageView icon = null;
        TextView league = null;
        int rowID = 0;

        RowHolder(View row) {
            super(row);
            teamView = (TextView) row.findViewById(R.id.textViewTeamName);
            match = (TextView) row.findViewById(R.id.textViewMatch);
            icon = (ImageView) row.findViewById(R.id.imageViewTeam);
            league = (TextView) row.findViewById(R.id.textViewLeagueName);
            row.setOnClickListener(this); //Waiting for a click
        }

        @Override
        public void onClick(View v) {
            Team team = SDH.getTeam(rowID);
            Intent intent = new Intent(MainActivity.this, TeamActivity.class);
            intent.putExtra(Team.TAG, team);
            startActivityForResult(intent, 2);
        }

        void bindModel(Cursor cursor) {
            rowID = cursor.getInt(cursor.getColumnIndex(SDH._ID));
            teamView.setText(cursor.getString(cursor.getColumnIndex(SDH.COLUMN_TEAM_NAME)));
            league.setText(cursor.getString(cursor.getColumnIndex(SDH.COLUMN_LEAGUE_NAME)));
            String infoMatch = cursor.getString(cursor.getColumnIndex(SDH.COLUMN_LAST_MATCH_LABEL)) + " : " + cursor.getString(cursor.getColumnIndex(SDH.COLUMN_LAST_MATCH_SCORE_HOME))+ " - " + cursor.getString(cursor.getColumnIndex(SDH.COLUMN_LAST_MATCH_SCORE_AWAY));
            if(infoMatch.equals("null : 0 - 0")) infoMatch = "";
            match.setText(infoMatch);
            File path = new File(MainActivity.this.getExternalFilesDir(null), cursor.getString(cursor.getColumnIndex(SDH.COLUMN_TEAM_NAME))+".png");
            Bitmap myBitmap = BitmapFactory.decodeFile(path.getAbsolutePath());
            icon.setImageBitmap(myBitmap);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class Worker extends AsyncTask<TextView, String, List<Team>> implements com.example.tp3.Worker {
        Request request;

        //Background thread
        @Override
        protected List<Team> doInBackground(TextView... textViews) {
            List<Team> listTeams = SDH.getAllTeams();
            for(Team team : listTeams)
            {
                request = new Request();
                boolean run = true;
                while(run == true)
                {
                    Team tempTeam = team;
                    tempTeam = request.getTeamInfo(tempTeam);
                    if(tempTeam == null)
                    {
                        break;
                    }
                    saveToInternalStorage(request.getImageFromURL(team.getTeamBadge()), team.getName());
                    team = request.getMatchInfo(tempTeam);
                    team = request.getRankingInfo(team);
                    run = false;
                }
            }
            return listTeams;
        }

        //UI thread
        @Override
        protected void onPostExecute(List<Team> listTeams) {
            super.onPostExecute(listTeams);
            for(Team team : listTeams)
            {
                SDH.updateTeam(team);
            }
            Cursor cursor = SDH.fetchAllTeams();
            adapter.changeCursor(cursor);
            SRL.setRefreshing(false); //Stop the refreshing animation
        }
    }

    private void saveToInternalStorage(Bitmap icon, String name)
    {
        File path = new File(this.getExternalFilesDir(null), name+".png");
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(path);
            // Use the compress method on the BitMap object to write image to the OutputStream
            icon.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.flush();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}